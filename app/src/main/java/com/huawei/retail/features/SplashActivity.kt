package com.huawei.retail.features

import android.os.Bundle
import android.os.Handler
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import com.huawei.retail.R
import com.huawei.retail.common.BaseActivity
import com.huawei.retail.features.dashboard.DashboardActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_splash)

        supportActionBar?.hide();

        Handler().postDelayed({
            startActivity(DashboardActivity.getStartIntent(this))
            finish()
        }, 2000)
    }
}
