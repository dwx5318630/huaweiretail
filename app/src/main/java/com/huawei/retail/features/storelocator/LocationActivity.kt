package com.huawei.retail.features.storelocator

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.huawei.hms.maps.*
import com.huawei.hms.maps.model.BitmapDescriptorFactory
import com.huawei.hms.maps.model.LatLng
import com.huawei.hms.maps.model.Marker
import com.huawei.hms.maps.model.MarkerOptions
import com.huawei.retail.R
import com.huawei.retail.common.API_KEY
import com.huawei.retail.common.BaseActivity
import com.huawei.retail.common.MAPVIEW_BUNDLE_KEY


class LocationActivity : BaseActivity(), OnMapReadyCallback {
    companion object {
        const val TAG = "Location"
        const val TAG_LOCATION_FRAGMENT = "LOCATION_FRAGMENT"
        private val BengaloreCoordinates = LatLng(12.9716, 77.5946)

        fun getStartIntent(context: Context): Intent {
            return Intent(context, LocationActivity::class.java)
        }
    }

    private var mMarkerPin: Marker? = null
    private var locationFragment =
        LocationFragment()
    private var hMap: HuaweiMap? = null
    private val mMapView: MapView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location)

        getDynamicPermissionForLocation()
        hasPermissions(this@LocationActivity, android.Manifest.permission.ACCESS_NETWORK_STATE)


        MapsInitializer.setApiKey(API_KEY)
        val mMapFragment: MapFragment =
            fragmentManager.findFragmentById(R.id.mapfragment_mapfragmentdemo) as MapFragment
        mMapFragment.getMapAsync(this)


        var mapViewBundle: Bundle? = null
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY)
        }
        mMapView?.onCreate(mapViewBundle)
        mMapView?.getMapAsync(this)
    }

    private fun injectLocationFragment(){
        supportFragmentManager.beginTransaction()
            .add(locationFragment, TAG_LOCATION_FRAGMENT).commit()
    }

    override fun onResume() {
        super.onResume()
        mMapView?.onResume()
        getCurrentLocation()
    }

    override fun onPause() {
        super.onPause()
        mMapView?.onPause()
        removeLocationUpdatesWithCallback()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mMapView?.onLowMemory()
    }

    override fun onStart() {
        super.onStart()
        mMapView?.onStart()
    }

    override fun onStop() {
        super.onStop()
        mMapView?.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        mMapView?.onDestroy()
    }

    @Override
    override fun onMapReady(map: HuaweiMap?) {
        Log.d(TAG, "onMapReady: ")
        hMap = map
        hMap?.mapType = HuaweiMap.MAP_TYPE_NORMAL
        hMap?.isMyLocationEnabled = true
        hMap?.uiSettings?.isMyLocationButtonEnabled = true
        hMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(12.9716, 77.5946), 14f))
        addMarker()
    }

    private fun addMarker() {
        if (null == hMap) {
            return
        }
        if (mMarkerPin == null) {
            // Uses a colored icon.
            mMarkerPin = hMap?.addMarker(
                    MarkerOptions().position(BengaloreCoordinates)
                        .title("paris").snippet("hello").clusterable(true).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_mappin))
                )

            hMap?.setOnMarkerClickListener { marker ->
                val clusterable = marker.isClusterable
                Toast.makeText(
                    applicationContext,
                    clusterable.toString(),
                    Toast.LENGTH_SHORT
                ).show()
                true
            }
        }

        // Add a marker when the point is long clicked.
        hMap?.setOnMapClickListener {
            Log.d(
                TAG,
                "Hi, You are at Huawei Retail store"
            )
        }
    }
}