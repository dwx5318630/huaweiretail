package com.huawei.retail.features.dashboard.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class CategoriesResponse(
    @SerializedName("message")
    val message:String,
    @SerializedName("status")
    val status: String,
    @SerializedName("data")
    val categories: List<Category>
) : Parcelable

@Parcelize
data class Category(
    @SerializedName("name")
    val name: String,
    @SerializedName("image_url")
    val imageUrl: String
) : Parcelable