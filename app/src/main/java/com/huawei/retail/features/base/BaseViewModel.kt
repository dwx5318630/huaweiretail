package com.huawei.retail.features.base

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

open class BaseViewModel(protected val applicationContext: Application):AndroidViewModel(applicationContext) {
    protected var errorLiveData = MutableLiveData<String>()
    protected var networkErrorLiveData = MutableLiveData<Boolean>()
    protected var progressLiveData = MutableLiveData<Boolean>()

    fun getErrorLiveData():LiveData<String>{
        return errorLiveData
    }

    fun getNetworkErrorLiveData():LiveData<Boolean>{
        return networkErrorLiveData
    }

    fun getProgressLiveData():LiveData<Boolean>{
        return progressLiveData
    }

    fun isNetworkOn():Boolean{
      //  if (Interne)
        return true
    }

    fun onError(exception:Throwable){
//        DataWrapper.error
    }
}