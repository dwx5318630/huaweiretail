package com.huawei.retail.features.dashboard

import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.huawei.hms.ads.AdParam
import com.huawei.retail.R
import com.huawei.retail.common.BaseActivity
import com.huawei.retail.features.dashboard.adapter.DashboardAdapter
import com.huawei.retail.features.dashboard.viewmodel.DashboardViewModel
import com.huawei.retail.features.storelocator.LocationActivity
import kotlinx.android.synthetic.main.activity_dashboard.*
import java.io.IOException
import java.util.*

class DashboardActivity : BaseActivity() {
    private lateinit var adapter: DashboardAdapter
    private lateinit var dashboardViewModel: DashboardViewModel

    companion object {

        fun getStartIntent(context: Context): Intent {
            return Intent(context, DashboardActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        val adParam = AdParam.Builder().build()
        BottomBannerView.loadAd(adParam)
    }

    override fun onStart() {
        super.onStart()

        dashboardViewModel = ViewModelProviders.of(this).get(DashboardViewModel::class.java)
        initObservers()

        dashboardViewModel.getCategories()
    }

    fun onSearchButtonClick(view: View) {
        startActivity(LocationActivity.getStartIntent(this))
    }

    private fun initObservers() {
        dashboardViewModel.getCategoryLiveData().observe(this, Observer {
            it?.run {
                adapter = DashboardAdapter(it, this@DashboardActivity)
                categoryRecyclerview.adapter = adapter
                categoryRecyclerview.layoutManager = GridLayoutManager(this@DashboardActivity, 2)
            }
        })
    }
}
