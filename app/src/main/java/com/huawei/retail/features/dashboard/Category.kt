package com.huawei.retail.features.dashboard

data class Category(val title: String, val iconResourceId: Int)