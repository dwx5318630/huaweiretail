package com.huawei.retail.features.dashboard.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.huawei.retail.R
import com.huawei.retail.features.dashboard.model.Category

class DashboardAdapter(private val categories: List<Category>, context:Context) : RecyclerView.Adapter<DashboardAdapter.ViewHolder>(), View.OnClickListener
{
    private lateinit var context: Context
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        val nameTextView: TextView = itemView.findViewById(R.id.name_textview)
        val categoryImageView: ImageView = itemView.findViewById(R.id.category_image)
        val cardView:CardView = itemView.findViewById(R.id.card_view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DashboardAdapter.ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)

        val contactView = inflater.inflate(R.layout.item_grid_category, parent, false)

        return ViewHolder(contactView)
    }

    override fun onBindViewHolder(viewHolder: DashboardAdapter.ViewHolder, position: Int) {
        val category = categories[position]
        viewHolder.nameTextView.text = category.name
        Glide.with(context).load(category.imageUrl).into(viewHolder.categoryImageView);
        viewHolder.cardView.tag = position
        viewHolder.cardView.setOnClickListener(this)
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    override fun onClick(view: View?) {
        when(view?.id){
            R.id.card_view->{
                val position = view.tag

            }
        }
    }
}