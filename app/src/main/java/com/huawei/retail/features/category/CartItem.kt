package com.huawei.retail.features.category

data class CartItem(var product: Product, var quantity: Int = 0)