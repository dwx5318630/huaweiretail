package com.huawei.retail.features.dashboard.viewmodel

import android.app.Application
import android.location.Address
import android.location.Geocoder
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.huawei.retail.data.network.Api
import com.huawei.retail.features.base.BaseViewModel
import com.huawei.retail.features.dashboard.model.Category
import kotlinx.coroutines.launch
import java.io.IOException
import java.util.*

class DashboardViewModel(applicationContext: Application) : BaseViewModel(applicationContext) {
    private val categoryLiveData = MutableLiveData<List<Category>>()

    fun getCategoryLiveData(): LiveData<List<Category>> {
        return categoryLiveData
    }

    fun getCategories() {
        viewModelScope.launch {
            progressLiveData.postValue(true)
            try {
                val response = Api.instance.getCategories()
                if (response != null) {
                    progressLiveData.postValue(false)
                    categoryLiveData.postValue(response.categories)
                } else {
                    progressLiveData.postValue(false)
                    errorLiveData.postValue("Error in getting response from server")
                }
            } catch (exception: Exception) {
                progressLiveData.postValue(false)
                errorLiveData.postValue("Error in getting response from server")
            }
        }
    }



}