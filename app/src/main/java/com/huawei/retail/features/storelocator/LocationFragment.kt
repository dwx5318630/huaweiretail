package com.huawei.retail.features.storelocator

import android.content.IntentSender
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.util.Log
import androidx.fragment.app.Fragment
import com.huawei.hms.common.ApiException
import com.huawei.hms.common.ResolvableApiException
import com.huawei.hms.location.*
import com.huawei.hms.maps.model.LatLng
import com.huawei.retail.common.API_KEY
import com.huawei.retail.common.LOCATION_REQUEST_INTERVAL
import com.huawei.retail.common.geocode.GeocodingService


class LocationFragment : Fragment() {

    companion object {
        const val TAG = "LocationFragment"
    }

    private lateinit var settingsClient: SettingsClient
    private var mLocationRequest = LocationRequest()
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var mLocationCallback: LocationCallback
    private var userLatLong = LatLng(0.0, 0.0)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        initLocationTracker()
    }

    override fun onResume() {
        super.onResume()
        getLocationUpdates()
    }

    private fun initLocationTracker() {
        // set the interval for location updates, in milliseconds.
        mLocationRequest.interval = LOCATION_REQUEST_INTERVAL

        // set the priority of the request
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private fun getLocationUpdates() {
        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                val locations: List<Location> = locationResult.locations
                if (locations.isNotEmpty()) {
                    for (location in locations) {
                        userLatLong.latitude = location.latitude
                        userLatLong.longitude = location.longitude

                        GeocodingService.reverseGeocoding(
                            API_KEY,
                            userLatLong.latitude,
                            userLatLong.longitude
                        )
                    }
                }
            }

            override fun onLocationAvailability(locationAvailability: LocationAvailability) {
                val flag = locationAvailability.isLocationAvailable
                Log.i(
                    TAG,
                    "onLocationAvailability isLocationAvailable:$flag"
                )
            }
        }

        getCurrentLocation()
    }

    fun getCurrentLocation() {
        try {
            val builder = LocationSettingsRequest.Builder()
            builder.addLocationRequest(mLocationRequest)
            val locationSettingsRequest = builder.build()

            settingsClient = LocationServices.getSettingsClient(activity)
            settingsClient.checkLocationSettings(locationSettingsRequest)
                .addOnSuccessListener {
                    Log.i(TAG, "check location settings success")
                    //request location updates
                    fusedLocationProviderClient =
                        LocationServices.getFusedLocationProviderClient(activity)
                    fusedLocationProviderClient.requestLocationUpdates(
                        mLocationRequest,
                        mLocationCallback,
                        Looper.getMainLooper()
                    ).addOnSuccessListener {
                        Log.i(
                            TAG,
                            "requestLocationUpdatesWithCallback onSuccess"
                        )


                    }.addOnFailureListener { e ->
                        Log.e(
                            TAG,
                            "requestLocationUpdatesWithCallback onFailure:" + e.message
                        )
                    }
                }
                .addOnFailureListener { e ->
                    Log.e(
                        TAG,
                        "checkLocationSetting onFailure:" + e.message
                    )
                    when ((e as ApiException).statusCode) {
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                            val rae = e as ResolvableApiException
                            rae.startResolutionForResult(activity, 0)
                        } catch (sie: IntentSender.SendIntentException) {
                            Log.e(TAG, "PendingIntent unable to execute request.")
                        }
                    }
                }
        } catch (e: Exception) {
            Log.e(
                TAG,
                "requestLocationUpdatesWithCallback exception:" + e.message
            )
        }
    }

    fun removeLocationUpdatesWithCallback() {
        try {
            fusedLocationProviderClient.removeLocationUpdates(mLocationCallback)
                .addOnSuccessListener {
                    Log.i(TAG, "removeLocationUpdatesWithCallback onSuccess")
                }
                .addOnFailureListener { e ->
                    Log.e(
                        TAG,
                        "removeLocationUpdatesWithCallback onFailure:" + e.message
                    )
                }
        } catch (e: Exception) {
            Log.e(TAG, "removeLocationUpdatesWithCallback exception:" + e.message)
        }
    }
}