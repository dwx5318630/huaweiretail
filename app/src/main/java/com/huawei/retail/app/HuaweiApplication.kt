package com.huawei.retail.app

import android.app.Application
import com.huawei.agconnect.crash.AGConnectCrash
import com.huawei.hms.ads.HwAds

class HuaweiApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        HwAds.init(this)
        //making crash collections enabled for this application
        AGConnectCrash.getInstance().enableCrashCollection(true)

            //Sampl123
    }
}