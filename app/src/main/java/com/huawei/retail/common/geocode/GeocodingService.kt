package com.huawei.retail.common.geocode

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.huawei.retail.common.ROOT_URL
import okhttp3.*

import org.json.JSONException

import org.json.JSONObject
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.net.URLEncoder


object GeocodingService {
    private const val connection = "?key="
    private val JSON: MediaType? = MediaType.parse("application/json; charset=utf-8")

    @RequiresApi(Build.VERSION_CODES.N)
    @Throws(UnsupportedEncodingException::class)
    fun reverseGeocoding(apiKey: String?, lat: Double, lon: Double) {
        val json = JSONObject()
        val location = JSONObject()
        try {
            Log.d("ReverseGeocodingResponse", lat.toString())
            Log.d("ReverseGeocodingResponse", lon.toString())

            location.put("lng", lat)
            location.put("lat", lon)
            json.put("location", location)
        } catch (e: JSONException) {
            Log.e("error", e.message)
        }
        val body = RequestBody.create(JSON, json.toString())
        val client = OkHttpClient()
        val request: Request = Request.Builder().url(
            ROOT_URL + "reverseGeocode" + connection + URLEncoder.encode(
                apiKey,
                "UTF-8"
            )
        )
            .post(body)
            .build()
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call?, e: IOException) {
                Log.e("ReverseGeocoding", e.toString())
            }


            @Throws(IOException::class)
            override fun onResponse(call: Call?, response: Response) {
                Log.d("ReverseGeocodingResponse", response.body()?.string())
            }
        })
    }
}