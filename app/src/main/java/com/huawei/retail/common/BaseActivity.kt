package com.huawei.retail.common

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.huawei.agconnect.crash.AGConnectCrash
import com.huawei.hms.common.ApiException
import com.huawei.hms.common.ResolvableApiException
import com.huawei.hms.location.*
import com.huawei.hms.maps.model.LatLng
import com.huawei.retail.common.geocode.GeocodingService
import com.huawei.retail.features.dashboard.DashboardActivity
import com.huawei.retail.features.storelocator.LocationActivity
import java.io.IOException
import java.util.*


@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {
    private lateinit var settingsClient: SettingsClient
    private var mLocationRequest = LocationRequest()
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var mLocationCallback: LocationCallback
    private var userLatLong = LatLng(0.0, 0.0)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mLocationRequest.interval = LOCATION_REQUEST_INTERVAL

        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    override fun onResume() {
        super.onResume()
        getLocationUpdates()
    }

    private fun getLocationUpdates() {
        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                val locations: List<Location> = locationResult.locations
                if (locations.isNotEmpty()) {
                    for (location in locations) {
                        userLatLong.latitude = 18.0527//location.latitude
                        userLatLong.longitude = 77.2155//location.longitude

                        GeocodingService.reverseGeocoding(
                            API_KEY,
                            userLatLong.latitude,
                            userLatLong.longitude
                        )


                    }
                }
            }

            override fun onLocationAvailability(locationAvailability: LocationAvailability) {
                val flag = locationAvailability.isLocationAvailable
                Log.i(
                    "Location",
                    "onLocationAvailability isLocationAvailable:$flag"
                )
            }
        }

        getCurrentLocation()
    }



    fun getDynamicPermissionForLocation() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            Log.i("Location", "sdk < 28 Q")
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                val strings = arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
                ActivityCompat.requestPermissions(this, strings, 1)
            }
        } else {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(
                    this,
                    "android.permission.ACCESS_BACKGROUND_LOCATION"
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                val strings = arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    "android.permission.ACCESS_BACKGROUND_LOCATION"
                )
                ActivityCompat.requestPermissions(this, strings, 2)
            }
        }
    }

    fun hasPermissions(
        context: Context,
        vararg permissions: String
    ): Boolean {
        for (permission in permissions) {
            if (ActivityCompat.checkSelfPermission(
                    context,
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return false
            }
        }
        return true
    }

    fun getCurrentLocation() {
        try {
            val builder = LocationSettingsRequest.Builder()
            builder.addLocationRequest(mLocationRequest)
            val locationSettingsRequest = builder.build()

            settingsClient = LocationServices.getSettingsClient(this)
            settingsClient.checkLocationSettings(locationSettingsRequest)
                .addOnSuccessListener {
                    Log.i(LocationActivity.TAG, "check location settings success")
                    //request location updates
                    fusedLocationProviderClient =
                        LocationServices.getFusedLocationProviderClient(this)
                    fusedLocationProviderClient.requestLocationUpdates(
                        mLocationRequest,
                        mLocationCallback,
                        Looper.getMainLooper()
                    ).addOnSuccessListener {
                        Log.i(
                            LocationActivity.TAG,
                            "requestLocationUpdatesWithCallback onSuccess"
                        )


                    }.addOnFailureListener { e ->
                        Log.e(
                            LocationActivity.TAG,
                            "requestLocationUpdatesWithCallback onFailure:" + e.message
                        )
                    }
                }
                .addOnFailureListener { e ->
                    Log.e(
                        LocationActivity.TAG,
                        "checkLocationSetting onFailure:" + e.message
                    )
                    when ((e as ApiException).statusCode) {
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                            val rae = e as ResolvableApiException
                            rae.startResolutionForResult(this, 0)
                        } catch (sie: IntentSender.SendIntentException) {
                            Log.e(LocationActivity.TAG, "PendingIntent unable to execute request.")
                        }
                    }
                }
        } catch (e: Exception) {
            Log.e(
                LocationActivity.TAG,
                "requestLocationUpdatesWithCallback exception:" + e.message
            )
        }
    }

    fun removeLocationUpdatesWithCallback() {
        try {
            fusedLocationProviderClient.removeLocationUpdates(mLocationCallback)
                .addOnSuccessListener {
                    Log.i(LocationActivity.TAG, "removeLocationUpdatesWithCallback onSuccess")
                }
                .addOnFailureListener { e ->
                    Log.e(
                        LocationActivity.TAG,
                        "removeLocationUpdatesWithCallback onFailure:" + e.message
                    )
                }
        } catch (e: java.lang.Exception) {
            Log.e(LocationActivity.TAG, "removeLocationUpdatesWithCallback exception:" + e.message)
        }
    }
}