package com.huawei.retail.data.network

import com.huawei.retail.features.dashboard.model.CategoriesResponse
import retrofit2.http.GET

interface RetrofitBuilder {

    //https://run.mocky.io/v3/08614bf8-5093-4c12-b788-f916b100bb7d
    @GET("08614bf8-5093-4c12-b788-f916b100bb7d")
    suspend fun getCategories(): CategoriesResponse?
}